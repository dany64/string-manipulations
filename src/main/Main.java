package main;

import java.text.Collator;

public class Main {

    public static void main(String[] args) {
        String s1 = "Apple";
        String s2 = "Mango";
        String s3 = "Apple";
        String s4 = new String("Apple");
        String s5 = new String (s4);
        String s6 = new String();
        String s7 = new String("");

        //Test of method intern()
        boolean test = "AppleMango" == (s1+s2).intern();
        System.out.println("test = " + test);

        String hello1 = "Hello";
        String hello2 = new String("Hello");
        boolean b1 = hello1 == hello2;

        String hello3 = "Hello";
        boolean b2 = hello1 == hello3;
        System.out.println("============================");
        System.out.println(hello1.compareTo(hello2));
        System.out.println("hello2".compareTo("hello1"));

        System.out.println(s5 == s4);
        System.out.println("============================");

        System.out.println(s6);
        System.out.println(s5 == s4);
        System.out.println(s6 == s4);
        System.out.println(s6.equals(s7));

        System.out.println("============================");
        System.out.println(s1);
        System.out.println(s3);

        s1 = "toto";
        System.out.println("Toto a faim il veut manger".toUpperCase());

        String crazy = new String(new String(new String(new String(new String(new String(new String(new String(new String("Mouahahahahahahahahahahahahahahahahahahahahha !!! I know I am crazy !!!")))))))));
        System.out.println("crazy = " + crazy);
        
        boolean c = "".isEmpty();
        System.out.println("c = " + c);

        boolean d = "".equals("");
        System.out.println("d = " + d);
        
        boolean e = "".length() == 0;
        System.out.println("e = " + e);


        String chaine = "Apple Apple";
        System.out.println("chaine.tot = " + chaine.indexOf('p'));
        System.out.println("chaine.tot = " + chaine.lastIndexOf('p'));
        System.out.println("chaine.tot = " + chaine.indexOf("pp"));
        System.out.println("chaine.tot = " + chaine.lastIndexOf("pp"));
        int i = 0;
        for (char val: chaine.toCharArray()
             ) {
            System.out.println("c["+i+"] = " + val);
            i++;
        }

        System.out.println("chaine = " + "Apple toto".replace("pp","to"));

        String total = String.join(",", s1,s2,s3,s4,s5,s6,s7);
        System.out.println("total = " + total);

        String status = "ACTIVE";
        switch (status){
            case "DRAFT" :
            case "ACTIVE" :
            case "DELETED" : {
                System.out.println("status = " + status);
                break;
            }
            default:{
                System.out.println("nothing choosed");
            }
        }

    StringBuilder sb = new StringBuilder(16);
       sb.append("toto");
       sb.append(" is ");
       sb.append(" a ");
       sb.append(" toto. ");
       sb.append(" Because he is going to school every morning and come back in the evening, he is very tired when it is time to go to bed");
        System.out.println("sb = " + sb);


        s1.equals("");
        s1 = "hEllO";
        swapCase(s1);


        Collator col = Collator.getInstance();
        s1 = "ab";
        s2 = "aB";
        int r = col.compare(s1,s2);
        if (r >0){
            System.out.println(s1+" comes after "+s2);
        }
        else  if (r <0){
            System.out.println(s1+" comes before "+s2);
        }
        else {
            System.out.println(s1+" is the same word as "+s2);
        }
        s1 = "Because he is going to school every morning and come back in the evening, he is very tired when";

        String[] sencenceWords = s1.split(" ");
        String longestWord = sencenceWords[0];
        for (int i1 = 1; i1 < sencenceWords.length; i1++) {
            if(longestWord.length() < sencenceWords[i].length())
                longestWord = sencenceWords[i];
        }

        System.out.println(longestWord);

        System.out.println(palindrome("toiyot"));

    }

    public static void swapCase(String s1) {
        String lower = s1.toLowerCase();
        String upper = s1.toUpperCase();
        char[] result = new char[s1.length()];

        for (int j = 0; j < s1.length() ; j++) {
            if(s1.charAt(j) == lower.charAt(j))
                result[j] = upper.charAt(j);
            else
                result[j] = lower.charAt(j);
        }

        System.out.println(String.copyValueOf(result));
    }


    public static boolean palindrome(String chaine){
        if(chaine == null)
            return true;
        if (chaine.length()<1)
            return true;

        chaine = chaine.toLowerCase();

        int i=0, j= chaine.length()-1;
        while (i<j){
            if (chaine.charAt(i) != chaine.charAt(j)){
                return false;
            }
            i++;
            j--;
        }
        return true;
    }




}

