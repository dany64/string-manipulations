package main;

public class Challenge3 {

    public static void main(String[] args) {

        String romanNumber ="XL";
        System.out.println(romanNumber +" = " + romanToArabic(romanNumber));

        String test = "a7 11";
        int result = sumNumbers(test);
        System.out.println("result = " + result);
        
        String toto = "abcde";
        System.out.println("stringSplosion(toto) = " + stringSplosion(toto));

        String s = "level";
        System.out.println("altPairs(s) = " + altPairs(s));

        int i = 0;
        int j = s.length() -1;
        while ((s.charAt(i) == s.charAt(j)) && (i < j)){
            i++;
            j--;
        }
        if (i == j){
            System.out.println("palindrome");
        }
        else{
            System.out.println("non palindrome");
        }
    }

    public  static int sumNumbers(String test){
        //String[] str = test.split("[^0-9]+|\" \"|\"\"");
        String[] str;
        if((""+test.charAt(0)).matches("[0-9]"))
            str = test.replaceAll("[^0-9]+|(\" \")+",",").split(",");
        else
            str = test.replaceAll("[^0-9]+|(\" \")+",",").substring(1).split(",");

        for (String s: str
             ) {
            if(s == "") continue;
            System.out.println("s = " + s);
        }
        System.out.println("str.length = " + str.length);
        int total = 0;
        for (String s: str
        ) {
            total += Integer.valueOf(s);
        }
        return total;
    }

    /**
     * A second version of the
     * @param value is the entry string.
     * @return the sum of the numbers appearing the string #value
     */
    public static int sumbNumbers2(String value){
        char[] chars = value.toCharArray();
        String numbers = "";
        boolean isNumber = false;
        for (int i = 0; i < chars.length ; i++) {
            String currentChar = ""+chars[i];
            if(currentChar.matches("[0-9]")){
                isNumber = true;
                numbers += currentChar;
            }
            else {
                if (isNumber){
                    isNumber = false;
                    numbers += ",";
                }
            }
        }
        System.out.println("numbers = " + numbers);
        String[] str = numbers.split(",");
        int total = 0;
        for (String s: str
        ) {
            total += Integer.valueOf(s);
        }
        return total;
    }

    /**
     *
     * @param chaine is the entry string.
     * @return the sum of each digit appearing in the string #chaine
     */
    public static int sumOfDigits(String chaine) {
        String str = chaine.replaceAll("[^0-9]","");
        char[] chars = str.toCharArray();
        int total = 0;
        for (char c: chars
             ) {
            total += Integer.valueOf("" + c);
        }
        return total;
    }


    /**
     * Write a program to convert roman numerals into their arabic equivalent. Here are the arabic equivalents for roman symbols:
     *
     * The "basic" roman symbols                                 The "auxiliary" roman symbols
     *
     * I         X      C       M                                      V     L      D
     *
     * 1        10     100    1000                                     5     50     500
     *
     * Convert the roman numeral to arabic processing the symbols from left to right according to the following rules:
     *
     * 1. A symbol following one of greater or equal value adds to its value. (E.g., XII = 12)
     *
     * 2. A symbol preceding one of greater value subtracts its value.(E.g., IV = 4; XL = 40)
     *
     * @param romanNumber
     * @return
     */
    public static int romanToArabic(String romanNumber){
        String str;
        int result = 0;
        char[] symbols = romanNumber.toCharArray();
        int currentSymbol = 0, previousSymbol = 0;
        for (int i = 0; i < symbols.length ; i++) {
            currentSymbol = getArabicValue(symbols[i]);
            if (currentSymbol == 0) {
                result = -1;
                break;
            }
            else{
                if(i == 0) {
                    result = currentSymbol;
                }
                else {
                    if (currentSymbol > previousSymbol){
                        result = (result - previousSymbol) +(currentSymbol - previousSymbol);
                    }
                    else {
                        result += currentSymbol;
                    }

                }
                previousSymbol = currentSymbol;
            }
        }
        return result;
    }



    public static int getArabicValue(char romanSymbol){
        switch (romanSymbol){
            case 'C':
            case 'c':
                return 100;
            case 'I':
            case 'i':
                return 1;
            case 'V':
            case 'v':
                return 5;
            case 'X':
            case 'x':
                return 10;
            case 'L':
            case 'l':
                return 50;
            case 'D':
            case 'd':
                return 500;
            case 'M':
            case 'm':
                return 1000;
            default:
                return 0;
        }
    }
/*
    public static int maxBlock(String str){
        int max =0;
        char c;
        char[] word = str.toCharArray();
        for (int i = 0; i < word.length; i++) {
            c = word[i];
           while (c)
        }
        return 0;
    }*/


    /**
     * Given a non-empty string like "Code" return a string like "CCoCodCode".
     * @param value
     * @return the repetition of the given value string as much as its length
     */
    public static String stringSplosion(String value){
        String result ="";
        for (int i = 0; i < value.length() ; i++) {
            result += value.substring(0,i+1);
        }
        return result;
    }

    /**
     * Given a string, return a string made of the chars at indexes 0,1, 4,5, 8,9...and so on.
     * @param str
     * @return
     */
    public static String altPairs(String str){
        String result = "";
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            int goodIndex = i%4;
            if (goodIndex == 0 || goodIndex == 1)
                result += chars[i];
        }
        return result;
    }
}
