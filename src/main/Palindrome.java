package main;

public class Palindrome {



    public static boolean isPalindrome(String chaine){
        if(chaine == null)
            throw new IllegalArgumentException("String cannot be null");

        if(chaine.length()<=1)
            return true;

        chaine =  chaine.toUpperCase();
        System.out.println("chaine = " + chaine);
        boolean flag = true;
        for (int i = 0,j = chaine.length()-1; i < j; i++, j--) {
                if(chaine.charAt(i) != chaine.charAt(j)){
                    flag = false;
                    break;
                }
        }
        return flag;
    }

    public static boolean isPalindrome2(String chaine){
        if(chaine == null)
            throw new IllegalArgumentException("String cannot be null");

        if(chaine.length()<=1)
            return true;

        chaine =  chaine.toUpperCase();
        System.out.println("chaine = " + chaine);

        boolean flag = true;
        int i = 0, j = chaine.length()-1;
        while (i < j){
            if(chaine.charAt(i) != chaine.charAt(j)){
                flag = false;
                break;
            }
            i++;
            j--;
        }
        return flag;
    }


    public static void main(String[] args) {
        boolean toto = isPalindrome("Toot");
        boolean toto2 = isPalindrome2("19991");
        System.out.println("toto = " + toto);
        System.out.println("toto = " + toto2);
    }
}
